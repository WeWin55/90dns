Copy `01-block-nintendo.sh` and `02-self-host-90dns.sh` to your Pi-hole machine and run them. The scripts has been tested on Raspbian 9.11 (stretch) with Pi-hole 4.3.2.

# Step 1. Block Nintendo domains

This config used official DNS servers from 90dns, and block other nintendo domains. (similar to 90dns config file for [dnsmasq](/dnsmasq/dnsmasq.conf))

`pihole-FTL` service will restart upon success.

```bash
bash 01-block-nintendo.sh
```

# Step 2. Self host 90dns on Pihole lighttpd (optional)

This step is optional but recommanded. As you are already using Pihole and setup an web server up and running. Self host 90dns will be much convenience in this case.

`pihole-FTL` and `lighttpd` service will restart upon success.

```bash
bash 02-self-host-90dns.sh
```
